-- Gui to Lua
-- Version: 3.2

-- Instances:

local Swordburst2 = Instance.new("ScreenGui")
local Frame = Instance.new("Frame")
local Frame_2 = Instance.new("Frame")
local TextButton = Instance.new("TextButton")
local TextLabel = Instance.new("TextLabel")
local TextLabel_2 = Instance.new("TextLabel")
local ScrollingFrame = Instance.new("ScrollingFrame")
local Frame_3 = Instance.new("Frame")
local TextLabel_3 = Instance.new("TextLabel")
local TextButton_2 = Instance.new("TextButton")
local Frame_4 = Instance.new("Frame")
local TextLabel_4 = Instance.new("TextLabel")
local TextButton_3 = Instance.new("TextButton")
local Frame_5 = Instance.new("Frame")
local TextLabel_5 = Instance.new("TextLabel")
local TextButton_4 = Instance.new("TextButton")
local Frame_6 = Instance.new("Frame")
local TextLabel_6 = Instance.new("TextLabel")
local TextButton_5 = Instance.new("TextButton")
local inp = Instance.new("TextBox")
local Frame_7 = Instance.new("Frame")
local TextLabel_7 = Instance.new("TextLabel")
local TextButton_6 = Instance.new("TextButton")
local inp_2 = Instance.new("TextBox")
local UIListLayout = Instance.new("UIListLayout")
local Frame_8 = Instance.new("Frame")
local TextLabel_8 = Instance.new("TextLabel")
local TextButton_7 = Instance.new("TextButton")
local inp_3 = Instance.new("TextBox")
local Frame_9 = Instance.new("Frame")
local TextLabel_9 = Instance.new("TextLabel")
local TextButton_8 = Instance.new("TextButton")
local inp_4 = Instance.new("TextBox")
local Frame_10 = Instance.new("Frame")
local TextLabel_10 = Instance.new("TextLabel")
local TextButton_9 = Instance.new("TextButton")
local inp_5 = Instance.new("TextBox")
local Frame_11 = Instance.new("Frame")
local TextLabel_11 = Instance.new("TextLabel")
local TextButton_10 = Instance.new("TextButton")
local ScrollingFrame_2 = Instance.new("ScrollingFrame")
local UIListLayout_2 = Instance.new("UIListLayout")
local F1 = Instance.new("TextButton")
local F2 = Instance.new("TextButton")
local F3 = Instance.new("TextButton")
local F4 = Instance.new("TextButton")
local F5 = Instance.new("TextButton")
local F7 = Instance.new("TextButton")
local F8 = Instance.new("TextButton")
local F9 = Instance.new("TextButton")
local F10 = Instance.new("TextButton")

--Properties:

Swordburst2.Name = "Swordburst 2"
Swordburst2.Parent = game.Players.LocalPlayer:WaitForChild("PlayerGui")
Swordburst2.ResetOnSpawn = false

Frame.Parent = Swordburst2
Frame.BackgroundColor3 = Color3.fromRGB(49, 49, 49)
Frame.BorderSizePixel = 0
Frame.Position = UDim2.new(0.378583968, 0, 0.283667624, 0)
Frame.Size = UDim2.new(0, 533, 0, 324)

Frame_2.Parent = Frame
Frame_2.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
Frame_2.BorderSizePixel = 0
Frame_2.Position = UDim2.new(-0.000774483546, 0, -0.00139542564, 0)
Frame_2.Size = UDim2.new(0, 534, 0, 57)

TextButton.Parent = Frame_2
TextButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextButton.BackgroundTransparency = 1.000
TextButton.Position = UDim2.new(0.894904733, 0, 0.0175438598, 0)
TextButton.Size = UDim2.new(0, 56, 0, 56)
TextButton.Font = Enum.Font.SourceSans
TextButton.Text = "X"
TextButton.TextColor3 = Color3.fromRGB(255, 60, 12)
TextButton.TextScaled = true
TextButton.TextSize = 14.000
TextButton.TextWrapped = true

TextLabel.Parent = Frame_2
TextLabel.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel.BackgroundTransparency = 1.000
TextLabel.Position = UDim2.new(0, 0, -0.00750652095, 0)
TextLabel.Size = UDim2.new(0, 488, 0, 56)
TextLabel.Font = Enum.Font.SciFi
TextLabel.Text = "Swordburst 2"
TextLabel.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel.TextScaled = true
TextLabel.TextSize = 14.000
TextLabel.TextWrapped = true

TextLabel_2.Parent = Frame
TextLabel_2.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
TextLabel_2.Position = UDim2.new(0.00374531839, 0, 0.826840043, 0)
TextLabel_2.Size = UDim2.new(0, 531, 0, 56)
TextLabel_2.Font = Enum.Font.SciFi
TextLabel_2.Text = "by 1Commandcracker"
TextLabel_2.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_2.TextScaled = true
TextLabel_2.TextSize = 14.000
TextLabel_2.TextWrapped = true

ScrollingFrame.Parent = Frame
ScrollingFrame.BackgroundColor3 = Color3.fromRGB(49, 49, 49)
ScrollingFrame.Position = UDim2.new(0.00265125744, 0, 0.174530506, 0)
ScrollingFrame.Size = UDim2.new(0, 421, 0, 210)
ScrollingFrame.CanvasPosition = Vector2.new(0, 300)
ScrollingFrame.CanvasSize = UDim2.new(0, 0, 2.75, 0)
ScrollingFrame.ScrollBarThickness = 10

Frame_3.Parent = ScrollingFrame
Frame_3.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
Frame_3.BackgroundTransparency = 0.500
Frame_3.BorderSizePixel = 0
Frame_3.Position = UDim2.new(-0.00157282082, 0, 0.0066249338, 0)
Frame_3.Size = UDim2.new(0, 410, 0, 78)

TextLabel_3.Parent = Frame_3
TextLabel_3.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_3.BackgroundTransparency = 1.000
TextLabel_3.Position = UDim2.new(0.0276422799, 0, 0.154424503, 0)
TextLabel_3.Size = UDim2.new(0, 200, 0, 50)
TextLabel_3.Font = Enum.Font.SciFi
TextLabel_3.Text = "Hit Box"
TextLabel_3.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_3.TextScaled = true
TextLabel_3.TextSize = 14.000
TextLabel_3.TextWrapped = true

TextButton_2.Parent = Frame_3
TextButton_2.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
TextButton_2.BorderSizePixel = 0
TextButton_2.Position = UDim2.new(0.72899729, 0, 0.154424429, 0)
TextButton_2.Size = UDim2.new(0, 97, 0, 50)
TextButton_2.Font = Enum.Font.SourceSans
TextButton_2.Text = "Off"
TextButton_2.TextColor3 = Color3.fromRGB(255, 255, 255)
TextButton_2.TextScaled = true
TextButton_2.TextSize = 14.000
TextButton_2.TextWrapped = true

Frame_4.Parent = ScrollingFrame
Frame_4.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
Frame_4.BackgroundTransparency = 0.500
Frame_4.BorderSizePixel = 0
Frame_4.Position = UDim2.new(-0.00157282082, 0, 0.0827146173, 0)
Frame_4.Size = UDim2.new(0, 410, 0, 78)

TextLabel_4.Parent = Frame_4
TextLabel_4.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_4.BackgroundTransparency = 1.000
TextLabel_4.Position = UDim2.new(0.0276422799, 0, 0.205706552, 0)
TextLabel_4.Size = UDim2.new(0, 200, 0, 50)
TextLabel_4.Font = Enum.Font.SciFi
TextLabel_4.Text = "Infinite Stamina"
TextLabel_4.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_4.TextScaled = true
TextLabel_4.TextSize = 14.000
TextLabel_4.TextWrapped = true

TextButton_3.Parent = Frame_4
TextButton_3.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
TextButton_3.BorderSizePixel = 0
TextButton_3.Position = UDim2.new(0.72899729, 0, 0.154424429, 0)
TextButton_3.Size = UDim2.new(0, 97, 0, 50)
TextButton_3.Font = Enum.Font.SourceSans
TextButton_3.Text = "Off"
TextButton_3.TextColor3 = Color3.fromRGB(255, 255, 255)
TextButton_3.TextScaled = true
TextButton_3.TextSize = 14.000
TextButton_3.TextWrapped = true

Frame_5.Parent = ScrollingFrame
Frame_5.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
Frame_5.BackgroundTransparency = 0.500
Frame_5.BorderSizePixel = 0
Frame_5.Position = UDim2.new(-0.00157282082, 0, 0.157797903, 0)
Frame_5.Size = UDim2.new(0, 410, 0, 78)

TextLabel_5.Parent = Frame_5
TextLabel_5.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_5.BackgroundTransparency = 1.000
TextLabel_5.Position = UDim2.new(0.0276422799, 0, 0.205706552, 0)
TextLabel_5.Size = UDim2.new(0, 200, 0, 50)
TextLabel_5.Font = Enum.Font.SciFi
TextLabel_5.Text = "Auto Farming"
TextLabel_5.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_5.TextScaled = true
TextLabel_5.TextSize = 14.000
TextLabel_5.TextWrapped = true

TextButton_4.Parent = Frame_5
TextButton_4.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
TextButton_4.BorderSizePixel = 0
TextButton_4.Position = UDim2.new(0.72899729, 0, 0.154424429, 0)
TextButton_4.Size = UDim2.new(0, 97, 0, 50)
TextButton_4.Font = Enum.Font.SourceSans
TextButton_4.Text = "Run"
TextButton_4.TextColor3 = Color3.fromRGB(255, 255, 255)
TextButton_4.TextScaled = true
TextButton_4.TextSize = 14.000
TextButton_4.TextWrapped = true

Frame_6.Parent = ScrollingFrame
Frame_6.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
Frame_6.BackgroundTransparency = 0.500
Frame_6.BorderSizePixel = 0
Frame_6.Position = UDim2.new(-0.00345327239, 0, 0.296259731, 0)
Frame_6.Size = UDim2.new(0, 410, 0, 78)

TextLabel_6.Parent = Frame_6
TextLabel_6.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_6.BackgroundTransparency = 1.000
TextLabel_6.Position = UDim2.new(0.022764327, 0, 0.141603917, 0)
TextLabel_6.Size = UDim2.new(0, 152, 0, 50)
TextLabel_6.Font = Enum.Font.SciFi
TextLabel_6.Text = "Speed"
TextLabel_6.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_6.TextScaled = true
TextLabel_6.TextSize = 14.000
TextLabel_6.TextWrapped = true

TextButton_5.Parent = Frame_6
TextButton_5.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
TextButton_5.BorderSizePixel = 0
TextButton_5.Position = UDim2.new(0.731436312, 0, 0.154424429, 0)
TextButton_5.Size = UDim2.new(0, 97, 0, 50)
TextButton_5.Font = Enum.Font.SourceSans
TextButton_5.Text = "Off"
TextButton_5.TextColor3 = Color3.fromRGB(255, 255, 255)
TextButton_5.TextScaled = true
TextButton_5.TextSize = 14.000
TextButton_5.TextWrapped = true

inp.Name = "inp"
inp.Parent = Frame_6
inp.BackgroundColor3 = Color3.fromRGB(57, 57, 57)
inp.BorderSizePixel = 0
inp.Position = UDim2.new(0.437293351, 0, 0.146492586, 0)
inp.Size = UDim2.new(0, 101, 0, 50)
inp.Font = Enum.Font.SciFi
inp.PlaceholderText = "16"
inp.Text = "100"
inp.TextColor3 = Color3.fromRGB(255, 255, 255)
inp.TextScaled = true
inp.TextSize = 14.000
inp.TextWrapped = true

Frame_7.Parent = ScrollingFrame
Frame_7.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
Frame_7.BackgroundTransparency = 0.500
Frame_7.BorderSizePixel = 0
Frame_7.Position = UDim2.new(-0.00345327239, 0, 0.429381549, 0)
Frame_7.Size = UDim2.new(0, 410, 0, 78)

TextLabel_7.Parent = Frame_7
TextLabel_7.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_7.BackgroundTransparency = 1.000
TextLabel_7.Position = UDim2.new(0.0300814006, 0, 0.154424429, 0)
TextLabel_7.Size = UDim2.new(0, 152, 0, 50)
TextLabel_7.Font = Enum.Font.SciFi
TextLabel_7.Text = "Jump"
TextLabel_7.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_7.TextScaled = true
TextLabel_7.TextSize = 14.000
TextLabel_7.TextWrapped = true

TextButton_6.Parent = Frame_7
TextButton_6.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
TextButton_6.BorderSizePixel = 0
TextButton_6.Position = UDim2.new(0.731436312, 0, 0.154424429, 0)
TextButton_6.Size = UDim2.new(0, 97, 0, 50)
TextButton_6.Font = Enum.Font.SourceSans
TextButton_6.Text = "Off"
TextButton_6.TextColor3 = Color3.fromRGB(255, 255, 255)
TextButton_6.TextScaled = true
TextButton_6.TextSize = 14.000
TextButton_6.TextWrapped = true

inp_2.Name = "inp"
inp_2.Parent = Frame_7
inp_2.BackgroundColor3 = Color3.fromRGB(57, 57, 57)
inp_2.BorderSizePixel = 0
inp_2.Position = UDim2.new(0.437293351, 0, 0.146492586, 0)
inp_2.Size = UDim2.new(0, 101, 0, 50)
inp_2.Font = Enum.Font.SciFi
inp_2.PlaceholderText = "50"
inp_2.Text = "100"
inp_2.TextColor3 = Color3.fromRGB(255, 255, 255)
inp_2.TextScaled = true
inp_2.TextSize = 14.000
inp_2.TextWrapped = true

UIListLayout.Parent = ScrollingFrame
UIListLayout.SortOrder = Enum.SortOrder.LayoutOrder
UIListLayout.VerticalAlignment = Enum.VerticalAlignment.Center
UIListLayout.Padding = UDim.new(0, 20)

Frame_8.Parent = ScrollingFrame
Frame_8.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
Frame_8.BackgroundTransparency = 0.500
Frame_8.BorderSizePixel = 0
Frame_8.Position = UDim2.new(-0.00345327239, 0, 0.429381549, 0)
Frame_8.Size = UDim2.new(0, 410, 0, 78)

TextLabel_8.Parent = Frame_8
TextLabel_8.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_8.BackgroundTransparency = 1.000
TextLabel_8.Position = UDim2.new(0.0300814006, 0, 0.154424429, 0)
TextLabel_8.Size = UDim2.new(0, 152, 0, 50)
TextLabel_8.Font = Enum.Font.SciFi
TextLabel_8.Text = "Dismantle All"
TextLabel_8.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_8.TextScaled = true
TextLabel_8.TextSize = 14.000
TextLabel_8.TextWrapped = true

TextButton_7.Parent = Frame_8
TextButton_7.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
TextButton_7.BorderSizePixel = 0
TextButton_7.Position = UDim2.new(0.731436312, 0, 0.154424429, 0)
TextButton_7.Size = UDim2.new(0, 97, 0, 50)
TextButton_7.Font = Enum.Font.SourceSans
TextButton_7.Text = "Run"
TextButton_7.TextColor3 = Color3.fromRGB(255, 255, 255)
TextButton_7.TextScaled = true
TextButton_7.TextSize = 14.000
TextButton_7.TextWrapped = true

inp_3.Name = "inp"
inp_3.Parent = Frame_8
inp_3.BackgroundColor3 = Color3.fromRGB(57, 57, 57)
inp_3.BorderSizePixel = 0
inp_3.Position = UDim2.new(0.437293351, 0, 0.146492586, 0)
inp_3.Size = UDim2.new(0, 101, 0, 50)
inp_3.Font = Enum.Font.SciFi
inp_3.PlaceholderText = "Name"
inp_3.Text = ""
inp_3.TextColor3 = Color3.fromRGB(255, 255, 255)
inp_3.TextScaled = true
inp_3.TextSize = 14.000
inp_3.TextWrapped = true

Frame_9.Parent = ScrollingFrame
Frame_9.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
Frame_9.BackgroundTransparency = 0.500
Frame_9.BorderSizePixel = 0
Frame_9.Position = UDim2.new(-0.00345327239, 0, 0.429381549, 0)
Frame_9.Size = UDim2.new(0, 410, 0, 78)

TextLabel_9.Parent = Frame_9
TextLabel_9.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_9.BackgroundTransparency = 1.000
TextLabel_9.Position = UDim2.new(0.0300814006, 0, 0.154424429, 0)
TextLabel_9.Size = UDim2.new(0, 152, 0, 50)
TextLabel_9.Font = Enum.Font.SciFi
TextLabel_9.Text = "Max Upgrade"
TextLabel_9.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_9.TextScaled = true
TextLabel_9.TextSize = 14.000
TextLabel_9.TextWrapped = true

TextButton_8.Parent = Frame_9
TextButton_8.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
TextButton_8.BorderSizePixel = 0
TextButton_8.Position = UDim2.new(0.731436312, 0, 0.154424429, 0)
TextButton_8.Size = UDim2.new(0, 97, 0, 50)
TextButton_8.Font = Enum.Font.SourceSans
TextButton_8.Text = "Run"
TextButton_8.TextColor3 = Color3.fromRGB(255, 255, 255)
TextButton_8.TextScaled = true
TextButton_8.TextSize = 14.000
TextButton_8.TextWrapped = true

inp_4.Name = "inp"
inp_4.Parent = Frame_9
inp_4.BackgroundColor3 = Color3.fromRGB(57, 57, 57)
inp_4.BorderSizePixel = 0
inp_4.Position = UDim2.new(0.437293351, 0, 0.146492586, 0)
inp_4.Size = UDim2.new(0, 101, 0, 50)
inp_4.Font = Enum.Font.SciFi
inp_4.PlaceholderText = "Name"
inp_4.Text = ""
inp_4.TextColor3 = Color3.fromRGB(255, 255, 255)
inp_4.TextScaled = true
inp_4.TextSize = 14.000
inp_4.TextWrapped = true

Frame_10.Parent = ScrollingFrame
Frame_10.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
Frame_10.BackgroundTransparency = 0.500
Frame_10.BorderSizePixel = 0
Frame_10.Position = UDim2.new(-0.00345327239, 0, 0.429381549, 0)
Frame_10.Size = UDim2.new(0, 410, 0, 78)

TextLabel_10.Parent = Frame_10
TextLabel_10.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_10.BackgroundTransparency = 1.000
TextLabel_10.Position = UDim2.new(0.0300814006, 0, 0.154424429, 0)
TextLabel_10.Size = UDim2.new(0, 152, 0, 50)
TextLabel_10.Font = Enum.Font.SciFi
TextLabel_10.Text = "Zoom Distance"
TextLabel_10.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_10.TextScaled = true
TextLabel_10.TextSize = 14.000
TextLabel_10.TextWrapped = true

TextButton_9.Parent = Frame_10
TextButton_9.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
TextButton_9.BorderSizePixel = 0
TextButton_9.Position = UDim2.new(0.731436312, 0, 0.154424429, 0)
TextButton_9.Size = UDim2.new(0, 97, 0, 50)
TextButton_9.Font = Enum.Font.SourceSans
TextButton_9.Text = "Off"
TextButton_9.TextColor3 = Color3.fromRGB(255, 255, 255)
TextButton_9.TextScaled = true
TextButton_9.TextSize = 14.000
TextButton_9.TextWrapped = true

inp_5.Name = "inp"
inp_5.Parent = Frame_10
inp_5.BackgroundColor3 = Color3.fromRGB(57, 57, 57)
inp_5.BorderSizePixel = 0
inp_5.Position = UDim2.new(0.437293351, 0, 0.146492586, 0)
inp_5.Size = UDim2.new(0, 101, 0, 50)
inp_5.Font = Enum.Font.SciFi
inp_5.PlaceholderText = "15"
inp_5.Text = "400"
inp_5.TextColor3 = Color3.fromRGB(255, 255, 255)
inp_5.TextScaled = true
inp_5.TextSize = 14.000
inp_5.TextWrapped = true

Frame_11.Parent = ScrollingFrame
Frame_11.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
Frame_11.BackgroundTransparency = 0.500
Frame_11.BorderSizePixel = 0
Frame_11.Position = UDim2.new(-0.00157282082, 0, 0.157797903, 0)
Frame_11.Size = UDim2.new(0, 410, 0, 78)

TextLabel_11.Parent = Frame_11
TextLabel_11.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_11.BackgroundTransparency = 1.000
TextLabel_11.Position = UDim2.new(0.0276422799, 0, 0.205706552, 0)
TextLabel_11.Size = UDim2.new(0, 200, 0, 50)
TextLabel_11.Font = Enum.Font.SciFi
TextLabel_11.Text = "Reach"
TextLabel_11.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_11.TextScaled = true
TextLabel_11.TextSize = 14.000
TextLabel_11.TextWrapped = true

TextButton_10.Parent = Frame_11
TextButton_10.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
TextButton_10.BorderSizePixel = 0
TextButton_10.Position = UDim2.new(0.72899729, 0, 0.154424429, 0)
TextButton_10.Size = UDim2.new(0, 97, 0, 50)
TextButton_10.Font = Enum.Font.SourceSans
TextButton_10.Text = "Run"
TextButton_10.TextColor3 = Color3.fromRGB(255, 255, 255)
TextButton_10.TextScaled = true
TextButton_10.TextSize = 14.000
TextButton_10.TextWrapped = true

ScrollingFrame_2.Parent = Frame
ScrollingFrame_2.BackgroundColor3 = Color3.fromRGB(49, 49, 49)
ScrollingFrame_2.Position = UDim2.new(0.790549278, 0, 0.174530506, 0)
ScrollingFrame_2.Size = UDim2.new(0, 111, 0, 210)
ScrollingFrame_2.CanvasPosition = Vector2.new(0, 150)

UIListLayout_2.Parent = ScrollingFrame_2
UIListLayout_2.HorizontalAlignment = Enum.HorizontalAlignment.Center
UIListLayout_2.SortOrder = Enum.SortOrder.LayoutOrder
UIListLayout_2.VerticalAlignment = Enum.VerticalAlignment.Center
UIListLayout_2.Padding = UDim.new(0, 20)

F1.Name = "F1"
F1.Parent = ScrollingFrame_2
F1.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
F1.BorderSizePixel = 0
F1.Size = UDim2.new(0, 50, 0, 50)
F1.Font = Enum.Font.SciFi
F1.Text = "F1"
F1.TextColor3 = Color3.fromRGB(255, 255, 255)
F1.TextScaled = true
F1.TextSize = 14.000
F1.TextWrapped = true

F2.Name = "F2"
F2.Parent = ScrollingFrame_2
F2.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
F2.BorderSizePixel = 0
F2.Size = UDim2.new(0, 50, 0, 50)
F2.Font = Enum.Font.SciFi
F2.Text = "F2"
F2.TextColor3 = Color3.fromRGB(255, 255, 255)
F2.TextScaled = true
F2.TextSize = 14.000
F2.TextWrapped = true

F3.Name = "F3"
F3.Parent = ScrollingFrame_2
F3.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
F3.BorderSizePixel = 0
F3.Size = UDim2.new(0, 50, 0, 50)
F3.Font = Enum.Font.SciFi
F3.Text = "F3"
F3.TextColor3 = Color3.fromRGB(255, 255, 255)
F3.TextScaled = true
F3.TextSize = 14.000
F3.TextWrapped = true

F4.Name = "F4"
F4.Parent = ScrollingFrame_2
F4.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
F4.BorderSizePixel = 0
F4.Size = UDim2.new(0, 50, 0, 50)
F4.Font = Enum.Font.SciFi
F4.Text = "F4"
F4.TextColor3 = Color3.fromRGB(255, 255, 255)
F4.TextScaled = true
F4.TextSize = 14.000
F4.TextWrapped = true

F5.Name = "F5"
F5.Parent = ScrollingFrame_2
F5.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
F5.BorderSizePixel = 0
F5.Size = UDim2.new(0, 50, 0, 50)
F5.Font = Enum.Font.SciFi
F5.Text = "F5"
F5.TextColor3 = Color3.fromRGB(255, 255, 255)
F5.TextScaled = true
F5.TextSize = 14.000
F5.TextWrapped = true

F7.Name = "F7"
F7.Parent = ScrollingFrame_2
F7.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
F7.BorderSizePixel = 0
F7.Size = UDim2.new(0, 50, 0, 50)
F7.Font = Enum.Font.SciFi
F7.Text = "F7"
F7.TextColor3 = Color3.fromRGB(255, 255, 255)
F7.TextScaled = true
F7.TextSize = 14.000
F7.TextWrapped = true

F8.Name = "F8"
F8.Parent = ScrollingFrame_2
F8.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
F8.BorderSizePixel = 0
F8.Size = UDim2.new(0, 50, 0, 50)
F8.Font = Enum.Font.SciFi
F8.Text = "F8"
F8.TextColor3 = Color3.fromRGB(255, 255, 255)
F8.TextScaled = true
F8.TextSize = 14.000
F8.TextWrapped = true

F9.Name = "F9"
F9.Parent = ScrollingFrame_2
F9.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
F9.BorderSizePixel = 0
F9.Size = UDim2.new(0, 50, 0, 50)
F9.Font = Enum.Font.SciFi
F9.Text = "F9"
F9.TextColor3 = Color3.fromRGB(255, 255, 255)
F9.TextScaled = true
F9.TextSize = 14.000
F9.TextWrapped = true

F10.Name = "F10"
F10.Parent = ScrollingFrame_2
F10.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
F10.BorderSizePixel = 0
F10.Size = UDim2.new(0, 50, 0, 50)
F10.Font = Enum.Font.SciFi
F10.Text = "F10"
F10.TextColor3 = Color3.fromRGB(255, 255, 255)
F10.TextScaled = true
F10.TextSize = 14.000
F10.TextWrapped = true

-- Scripts:

local function LRDOQV_fake_script() -- Frame.Drag 
	local script = Instance.new('LocalScript', Frame)

	local frame = script.Parent
	frame.Draggable = true 
	frame.Active = true 
	frame.Selectable = true
end
coroutine.wrap(LRDOQV_fake_script)()
local function ZUAYG_fake_script() -- TextButton.LocalScript 
	local script = Instance.new('LocalScript', TextButton)

	local button = script.Parent
	local gui = button.Parent.Parent.Parent
	
	button.MouseButton1Click:Connect(function()
		gui:Destroy()
	end)
	
end
coroutine.wrap(ZUAYG_fake_script)()
local function YRRVVPX_fake_script() -- TextButton_2.LocalScript 
	local script = Instance.new('LocalScript', TextButton_2)

	local player = game:GetService("Players").LocalPlayer
	Toggle = false
	local button = script.Parent
	
	button.MouseButton1Click:Connect(function()
		if Toggle then
			Toggle = false
			button.Text = "Off"
			for i,v in pairs(game.Workspace.Mobs:children()) do
				if v:FindFirstChild("Head") then
					v.HumanoidRootPart.Transparency = 1
					v.HumanoidRootPart.Size = Vector3.new(10, 10, 10)
				end
			end
		else
			Toggle = true
			button.Text = "On"
		end
		while Toggle do
			for i,v in pairs(game.Workspace.Mobs:children()) do
				if v:FindFirstChild("Head") then
					v.HumanoidRootPart.Transparency = 0.25
					v.HumanoidRootPart.Size = Vector3.new(30, 25, 30)
					wait()
					if Toggle == false then break end
				end
			end
			wait()
			if Toggle == false then break end
		end
	end)
	
end
coroutine.wrap(YRRVVPX_fake_script)()
local function ICDIZ_fake_script() -- TextButton_3.LocalScript 
	local script = Instance.new('LocalScript', TextButton_3)

	local player = game:GetService("Players").LocalPlayer
	Toggle = false
	local button = script.Parent
	
	button.MouseButton1Click:Connect(function()
		if Toggle then
			Toggle = false
			button.Text = "Off"
		else
			Toggle = true
			button.Text = "On"
		end
		while Toggle do
			if player.Character then
				player.Character:WaitForChild("Entity").Stamina.Value = 100
				wait()
				if Toggle == false then break end
			end
		end
	end)
	
end
coroutine.wrap(ICDIZ_fake_script)()
local function EJJID_fake_script() -- TextButton_4.LocalScript 
	local script = Instance.new('LocalScript', TextButton_4)

	script.Parent.MouseButton1Click:Connect(function()
		loadstring(game:HttpGet(('https://pastebin.com/raw/4Be6xfSz'),true))()
	end)
end
coroutine.wrap(EJJID_fake_script)()
local function GNSVQCY_fake_script() -- TextButton_5.LocalScript 
	local script = Instance.new('LocalScript', TextButton_5)

	local player = game:GetService("Players").LocalPlayer
	Toggle = false
	local button = script.Parent
	local inp = button.Parent.inp
	
	button.MouseButton1Click:Connect(function()
		if Toggle then
			Toggle = false
			button.Text = "Off"
			player.Character:WaitForChild("Humanoid").WalkSpeed = 16
		else
			Toggle = true
			button.Text = "On"
			player.Character:WaitForChild("Humanoid").WalkSpeed = inp.Text
		end
	end)
	
	player.CharacterAdded:Connect(function(character)
		if Toggle then
			player.Character:WaitForChild("Humanoid").WalkSpeed = inp.Text
			wait(0.5)
		end
	end)
	
end
coroutine.wrap(GNSVQCY_fake_script)()
local function QEFT_fake_script() -- TextButton_6.LocalScript 
	local script = Instance.new('LocalScript', TextButton_6)

	local player = game:GetService("Players").LocalPlayer
	Toggle = false
	local button = script.Parent
	local inp = button.Parent.inp
	
	button.MouseButton1Click:Connect(function()
		if Toggle then
			Toggle = false
			button.Text = "Off"
			player.Character:WaitForChild("Humanoid").JumpPower = 50
		else
			Toggle = true
			button.Text = "On"
			player.Character:WaitForChild("Humanoid").JumpPower = inp.Text
		end
	end)
	
	
	player.CharacterAdded:Connect(function(character)
		if Toggle then
			player.Character:WaitForChild("Humanoid").JumpPower = inp.Text
			wait(0.5)
		end
	end)
	
end
coroutine.wrap(QEFT_fake_script)()
local function CZASBHM_fake_script() -- TextButton_7.LocalScript 
	local script = Instance.new('LocalScript', TextButton_7)

	local player = game:GetService("Players").LocalPlayer
	local button = script.Parent
	local inp = button.Parent.inp
	local PlayerName = tostring(player.Name)
	
	button.MouseButton1Click:Connect(function()
		local SelectedWeaponData = inp.Text
		for i,v in pairs(game.ReplicatedStorage.Profiles[PlayerName].Inventory:GetChildren()) do
			if v.Name == (SelectedWeaponData) then
				game.ReplicatedStorage.Event:FireServer("Equipment", {"Dismantle", v})
				local check = v:FindFirstChild(SelectedWeaponData)
				if not check then
	       			print("Dismantled " .. SelectedWeaponData ..".")
				end
			end
		end
	end)
	
end
coroutine.wrap(CZASBHM_fake_script)()
local function AZIVL_fake_script() -- TextButton_8.LocalScript 
	local script = Instance.new('LocalScript', TextButton_8)

	local player = game:GetService("Players").LocalPlayer
	local button = script.Parent
	local inp = button.Parent.inp
	local PlayerName = tostring(player.Name)
	
	button.MouseButton1Click:Connect(function()
	   local SelectedWeaponData = inp.Text
	     for i,v in pairs(game.ReplicatedStorage.Profiles[PlayerName].Inventory:GetChildren()) do
	           if v.Name == (SelectedWeaponData) then
	               for i=1,100 do
	                   game.ReplicatedStorage.Event:FireServer("Equipment", {"Upgrade", v, false})
	               print("Upgraded " .. SelectedWeaponData .. ".")
	           end
	       end
	   end
	end)
	
end
coroutine.wrap(AZIVL_fake_script)()
local function OKFBODM_fake_script() -- TextButton_9.LocalScript 
	local script = Instance.new('LocalScript', TextButton_9)

	local player = game:GetService("Players").LocalPlayer
	Toggle = false
	local button = script.Parent
	local inp = button.Parent.inp
	
	button.MouseButton1Click:Connect(function()
		if Toggle then
			Toggle = false
			button.Text = "Off"
			player.CameraMaxZoomDistance = 15
		else
			Toggle = true
			button.Text = "On"
			player.CameraMaxZoomDistance = inp.Text
		end
	end)
	
end
coroutine.wrap(OKFBODM_fake_script)()
local function MCPZRCJ_fake_script() -- TextButton_10.Script 
	local script = Instance.new('Script', TextButton_10)

	local player = game:GetService("Players").LocalPlayer
	
	script.Parent.MouseButton1Click:Connect(function()
		if player.Character then -- by 1Commandcracker
			for i,p in pairs(player.Character:GetChildren()) do
				if p.Name == "RightWeapon" or p.Name == "LeftWeapon" then
					for i,v in pairs(p.Tool:GetChildren()) do
						v.Size = Vector3.new(50, 50, 50)
						v.Transparency = 1
					end
				end
			end
		end
	end)
	
end
coroutine.wrap(MCPZRCJ_fake_script)()
local function KOWZS_fake_script() -- F1.LocalScript 
	local script = Instance.new('LocalScript', F1)

	script.Parent.MouseButton1Click:Connect(function()
		loadstring(game:HttpGet(("https://gitlab.com/Commandcracker/roblox-swordburst-2-gui/-/raw/master/Virhst%20Woodlands.lua"),true))()
	end)
end
coroutine.wrap(KOWZS_fake_script)()
local function CATLZ_fake_script() -- F2.LocalScript 
	local script = Instance.new('LocalScript', F2)

	script.Parent.MouseButton1Click:Connect(function()
		loadstring(game:HttpGet(("https://gitlab.com/Commandcracker/roblox-swordburst-2-gui/-/raw/master/Redveil%20Grove.lua"),true))()
	end)
end
coroutine.wrap(CATLZ_fake_script)()
local function ETCQYBN_fake_script() -- F3.LocalScript 
	local script = Instance.new('LocalScript', F3)

	script.Parent.MouseButton1Click:Connect(function()
		loadstring(game:HttpGet(("https://gitlab.com/Commandcracker/roblox-swordburst-2-gui/-/raw/master/Avalanche%20Expanse.lua"),true))()
	end)
end
coroutine.wrap(ETCQYBN_fake_script)()
local function IUMQ_fake_script() -- F4.LocalScript 
	local script = Instance.new('LocalScript', F4)

	script.Parent.MouseButton1Click:Connect(function()
		loadstring(game:HttpGet(("https://gitlab.com/Commandcracker/roblox-swordburst-2-gui/-/raw/master/Hidden%20Wilds.lua"),true))()
	end)
end
coroutine.wrap(IUMQ_fake_script)()
local function DENG_fake_script() -- F5.LocalScript 
	local script = Instance.new('LocalScript', F5)

	script.Parent.MouseButton1Click:Connect(function()
		loadstring(game:HttpGet(("https://gitlab.com/Commandcracker/roblox-swordburst-2-gui/-/raw/master/Desolate%20Dunes.lua"),true))()
	end)
end
coroutine.wrap(DENG_fake_script)()
local function VDOXU_fake_script() -- F7.LocalScript 
	local script = Instance.new('LocalScript', F7)

	script.Parent.MouseButton1Click:Connect(function()
		loadstring(game:HttpGet(("https://gitlab.com/Commandcracker/roblox-swordburst-2-gui/-/raw/master/Entoloma%20Gloomlands.lua"),true))()
	end)
end
coroutine.wrap(VDOXU_fake_script)()
local function QMJPXWT_fake_script() -- F8.LocalScript 
	local script = Instance.new('LocalScript', F8)

	script.Parent.MouseButton1Click:Connect(function()
		loadstring(game:HttpGet(("https://gitlab.com/Commandcracker/roblox-swordburst-2-gui/-/raw/master/Blooming%20Plateau.lua"),true))()
	end)
end
coroutine.wrap(QMJPXWT_fake_script)()
local function LSWP_fake_script() -- F9.LocalScript 
	local script = Instance.new('LocalScript', F9)

	script.Parent.MouseButton1Click:Connect(function()
		loadstring(game:HttpGet(("https://gitlab.com/Commandcracker/roblox-swordburst-2-gui/-/raw/master/Va'%20Rok.lua"),true))()
	end)
end
coroutine.wrap(LSWP_fake_script)()
local function VWQWF_fake_script() -- F10.LocalScript 
	local script = Instance.new('LocalScript', F10)

	script.Parent.MouseButton1Click:Connect(function()
		loadstring(game:HttpGet(('https://gitlab.com/Commandcracker/roblox-swordburst-2-gui/-/raw/master/Transylvania.lua'),true))()
	end)
end
coroutine.wrap(VWQWF_fake_script)()
