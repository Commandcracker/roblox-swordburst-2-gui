-- Gui to Lua
-- Version: 3.2

-- Instances:

local AvalancheExpanse = Instance.new("ScreenGui")
local Frame = Instance.new("Frame")
local ImageButton = Instance.new("ImageButton")
local ImageLabel = Instance.new("ImageLabel")
local TextLabel = Instance.new("TextLabel")
local ImageButton_2 = Instance.new("ImageButton")
local TextLabel_2 = Instance.new("TextLabel")
local Frame_2 = Instance.new("Frame")
local TextButton = Instance.new("TextButton")
local TextLabel_3 = Instance.new("TextLabel")
local TextLabel_4 = Instance.new("TextLabel")
local ImageButton_3 = Instance.new("ImageButton")

--Properties:

AvalancheExpanse.Name = "Avalanche Expanse"
AvalancheExpanse.Parent = game.Players.LocalPlayer:WaitForChild("PlayerGui")
AvalancheExpanse.ZIndexBehavior = Enum.ZIndexBehavior.Sibling

Frame.Parent = AvalancheExpanse
Frame.BackgroundColor3 = Color3.fromRGB(49, 49, 49)
Frame.BorderSizePixel = 0
Frame.Position = UDim2.new(0.697483897, 0, 0.259640008, 0)
Frame.Size = UDim2.new(0, 386, 0, 452)

ImageButton.Parent = Frame
ImageButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
ImageButton.BorderSizePixel = 0
ImageButton.Position = UDim2.new(0.706385136, 0, 0.261225283, 0)
ImageButton.Size = UDim2.new(0, 100, 0, 282)
ImageButton.Image = "http://www.roblox.com/asset/?id=4744822917"

ImageLabel.Parent = Frame
ImageLabel.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
ImageLabel.BorderSizePixel = 0
ImageLabel.Position = UDim2.new(0.0860185549, 0, 0.15129374, 0)
ImageLabel.Size = UDim2.new(0, 220, 0, 150)
ImageLabel.Image = "http://www.roblox.com/asset/?id=4744822219"

TextLabel.Parent = Frame
TextLabel.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
TextLabel.BorderSizePixel = 0
TextLabel.Position = UDim2.new(0.705407739, 0, 0.150605798, 0)
TextLabel.Size = UDim2.new(0, 102, 0, 50)
TextLabel.Font = Enum.Font.SciFi
TextLabel.Text = "Ra'thae the Ice King"
TextLabel.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel.TextScaled = true
TextLabel.TextSize = 14.000
TextLabel.TextWrapped = true

ImageButton_2.Parent = Frame
ImageButton_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
ImageButton_2.BorderSizePixel = 0
ImageButton_2.Position = UDim2.new(0.0688700154, 0, 0.663808286, 0)
ImageButton_2.Size = UDim2.new(0, 100, 0, 100)
ImageButton_2.Image = "http://www.roblox.com/asset/?id=4744822434"

TextLabel_2.Parent = Frame
TextLabel_2.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
TextLabel_2.BorderColor3 = Color3.fromRGB(27, 42, 53)
TextLabel_2.BorderSizePixel = 0
TextLabel_2.Position = UDim2.new(0.0691441447, 0, 0.554182827, 0)
TextLabel_2.Size = UDim2.new(0, 100, 0, 50)
TextLabel_2.Font = Enum.Font.SciFi
TextLabel_2.Text = "Alpha Icewhal"
TextLabel_2.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_2.TextScaled = true
TextLabel_2.TextSize = 14.000
TextLabel_2.TextWrapped = true

Frame_2.Parent = Frame
Frame_2.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
Frame_2.BorderSizePixel = 0
Frame_2.Position = UDim2.new(0.00321541424, 0, 0.00111733493, 0)
Frame_2.Size = UDim2.new(0, 384, 0, 45)

TextButton.Parent = Frame_2
TextButton.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
TextButton.BackgroundTransparency = 1.000
TextButton.BorderSizePixel = 0
TextButton.Position = UDim2.new(0.847231686, 0, 0.0350885391, 0)
TextButton.Size = UDim2.new(0, 47, 0, 45)
TextButton.Font = Enum.Font.SourceSans
TextButton.Text = "X"
TextButton.TextColor3 = Color3.fromRGB(255, 60, 12)
TextButton.TextScaled = true
TextButton.TextSize = 14.000
TextButton.TextWrapped = true

TextLabel_3.Parent = Frame_2
TextLabel_3.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_3.BackgroundTransparency = 1.000
TextLabel_3.Position = UDim2.new(-0.00130907691, 0, 0.0240715872, 0)
TextLabel_3.Size = UDim2.new(0, 325, 0, 45)
TextLabel_3.Font = Enum.Font.SciFi
TextLabel_3.Text = "Avalanche Expanse"
TextLabel_3.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_3.TextScaled = true
TextLabel_3.TextSize = 14.000
TextLabel_3.TextWrapped = true

TextLabel_4.Parent = Frame
TextLabel_4.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
TextLabel_4.BorderSizePixel = 0
TextLabel_4.Position = UDim2.new(0.389909744, 0, 0.551048279, 0)
TextLabel_4.Size = UDim2.new(0, 102, 0, 50)
TextLabel_4.Font = Enum.Font.SciFi
TextLabel_4.Text = "Qerach The Forgotten Golem"
TextLabel_4.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_4.TextScaled = true
TextLabel_4.TextSize = 14.000
TextLabel_4.TextWrapped = true

ImageButton_3.Parent = Frame
ImageButton_3.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
ImageButton_3.BorderSizePixel = 0
ImageButton_3.Position = UDim2.new(0.395454556, 0, 0.664719641, 0)
ImageButton_3.Size = UDim2.new(0, 100, 0, 100)
ImageButton_3.Image = "http://www.roblox.com/asset/?id=4744822623"

-- Scripts:

local function WCWCN_fake_script() -- ImageButton.LocalScript 
	local script = Instance.new('LocalScript', ImageButton)

	script.Parent.MouseButton1Click:Connect(function()
		game.Players.LocalPlayer.Character:MoveTo(Vector3.new(-380, 4167, -661))
	end)
	
end
coroutine.wrap(WCWCN_fake_script)()
local function FAJYV_fake_script() -- ImageButton_2.LocalScript 
	local script = Instance.new('LocalScript', ImageButton_2)

	script.Parent.MouseButton1Click:Connect(function()
		game.Players.LocalPlayer.Character:MoveTo(Vector3.new(1101, 6640, 2168))
	end)
	
end
coroutine.wrap(FAJYV_fake_script)()
local function DFEDRAV_fake_script() -- Frame.Drag 
	local script = Instance.new('LocalScript', Frame)

	local frame = script.Parent
	frame.Draggable = true 
	frame.Active = true 
	frame.Selectable = true
end
coroutine.wrap(DFEDRAV_fake_script)()
local function CHEN_fake_script() -- TextButton.LocalScript 
	local script = Instance.new('LocalScript', TextButton)

	local button = script.Parent
	local gui = button.Parent.Parent.Parent
	
	button.MouseButton1Click:Connect(function()
		gui:Destroy()
	end)
	
end
coroutine.wrap(CHEN_fake_script)()
local function THWRQQ_fake_script() -- ImageButton_3.LocalScript 
	local script = Instance.new('LocalScript', ImageButton_3)

	script.Parent.MouseButton1Click:Connect(function()
		game.Players.LocalPlayer.Character:MoveTo(Vector3.new(-381, 4065, -876))
	end)
	
end
coroutine.wrap(THWRQQ_fake_script)()
