-- Gui to Lua
-- Version: 3.2

-- Instances:

local VirhstWoodlands = Instance.new("ScreenGui")
local Frame = Instance.new("Frame")
local ImageButton = Instance.new("ImageButton")
local ImageLabel = Instance.new("ImageLabel")
local TextLabel = Instance.new("TextLabel")
local ImageButton_2 = Instance.new("ImageButton")
local TextLabel_2 = Instance.new("TextLabel")
local Frame_2 = Instance.new("Frame")
local TextButton = Instance.new("TextButton")
local TextLabel_3 = Instance.new("TextLabel")

--Properties:

VirhstWoodlands.Name = "Virhst Woodlands"
VirhstWoodlands.Parent = game.Players.LocalPlayer:WaitForChild("PlayerGui")
VirhstWoodlands.ZIndexBehavior = Enum.ZIndexBehavior.Sibling

Frame.Parent = VirhstWoodlands
Frame.BackgroundColor3 = Color3.fromRGB(49, 49, 49)
Frame.BorderSizePixel = 0
Frame.Position = UDim2.new(0.706846118, 0, 0.255008548, 0)
Frame.Size = UDim2.new(0, 311, 0, 452)

ImageButton.Parent = Frame
ImageButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
ImageButton.BorderSizePixel = 0
ImageButton.Position = UDim2.new(0.551521897, 0, 0.700117886, 0)
ImageButton.Size = UDim2.new(0, 100, 0, 100)
ImageButton.Image = "http://www.roblox.com/asset/?id=4744883515"

ImageLabel.Parent = Frame
ImageLabel.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
ImageLabel.BorderSizePixel = 0
ImageLabel.Position = UDim2.new(0.17029953, 0, 0.186691985, 0)
ImageLabel.Size = UDim2.new(0, 220, 0, 150)
ImageLabel.Image = "http://www.roblox.com/asset/?id=4744883125"

TextLabel.Parent = Frame
TextLabel.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
TextLabel.BorderSizePixel = 0
TextLabel.Position = UDim2.new(0.549839199, 0, 0.588658929, 0)
TextLabel.Size = UDim2.new(0, 102, 0, 50)
TextLabel.Font = Enum.Font.SciFi
TextLabel.Text = "Rahjin the Thief King"
TextLabel.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel.TextScaled = true
TextLabel.TextSize = 14.000
TextLabel.TextWrapped = true

ImageButton_2.Parent = Frame
ImageButton_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
ImageButton_2.BorderSizePixel = 0
ImageButton_2.Position = UDim2.new(0.130455703, 0, 0.701418936, 0)
ImageButton_2.Size = UDim2.new(0, 100, 0, 100)
ImageButton_2.Image = "http://www.roblox.com/asset/?id=4744883336"

TextLabel_2.Parent = Frame
TextLabel_2.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
TextLabel_2.BorderColor3 = Color3.fromRGB(27, 42, 53)
TextLabel_2.BorderSizePixel = 0
TextLabel_2.Position = UDim2.new(0.130096525, 0, 0.587368667, 0)
TextLabel_2.Size = UDim2.new(0, 100, 0, 50)
TextLabel_2.Font = Enum.Font.SciFi
TextLabel_2.Text = "Dire Wolf"
TextLabel_2.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_2.TextScaled = true
TextLabel_2.TextSize = 14.000
TextLabel_2.TextWrapped = true

Frame_2.Parent = Frame
Frame_2.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
Frame_2.BorderSizePixel = 0
Frame_2.Position = UDim2.new(0.00321543217, 0, 0.00111734867, 0)
Frame_2.Size = UDim2.new(0, 310, 0, 45)

TextButton.Parent = Frame_2
TextButton.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
TextButton.BackgroundTransparency = 1.000
TextButton.BorderSizePixel = 0
TextButton.Position = UDim2.new(0.847231686, 0, 0.0350885391, 0)
TextButton.Size = UDim2.new(0, 47, 0, 45)
TextButton.Font = Enum.Font.SourceSans
TextButton.Text = "X"
TextButton.TextColor3 = Color3.fromRGB(255, 60, 12)
TextButton.TextScaled = true
TextButton.TextSize = 14.000
TextButton.TextWrapped = true

TextLabel_3.Parent = Frame_2
TextLabel_3.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_3.BackgroundTransparency = 1.000
TextLabel_3.Position = UDim2.new(-0.00159479678, 0, -0.0203723907, 0)
TextLabel_3.Size = UDim2.new(0, 262, 0, 45)
TextLabel_3.Font = Enum.Font.SciFi
TextLabel_3.Text = "Virhst Woodlands"
TextLabel_3.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_3.TextScaled = true
TextLabel_3.TextSize = 14.000
TextLabel_3.TextWrapped = true

-- Scripts:

local function HMJC_fake_script() -- ImageButton.LocalScript 
	local script = Instance.new('LocalScript', ImageButton)

	script.Parent.MouseButton1Click:Connect(function()
		game.Players.LocalPlayer.Character:MoveTo(Vector3.new(-1933, 319.8, 1874.7))
	end)
	
end
coroutine.wrap(HMJC_fake_script)()
local function TJUTL_fake_script() -- ImageButton_2.LocalScript 
	local script = Instance.new('LocalScript', ImageButton_2)

	script.Parent.MouseButton1Click:Connect(function()
		game.Players.LocalPlayer.Character:MoveTo(Vector3.new(773, 641.8, 1244.68))
	end)
	
end
coroutine.wrap(TJUTL_fake_script)()
local function YUUBGG_fake_script() -- Frame.Drag 
	local script = Instance.new('LocalScript', Frame)

	local frame = script.Parent
	frame.Draggable = true 
	frame.Active = true 
	frame.Selectable = true
end
coroutine.wrap(YUUBGG_fake_script)()
local function LFMP_fake_script() -- TextButton.LocalScript 
	local script = Instance.new('LocalScript', TextButton)

	local button = script.Parent
	local gui = button.Parent.Parent.Parent
	
	button.MouseButton1Click:Connect(function()
		gui:Destroy()
	end)
	
end
coroutine.wrap(LFMP_fake_script)()
